#!/bin/bash
set -e

# general
apt-get install -y wget

# manaplus

sh -c 'echo "deb-src http://de.archive.ubuntu.com/ubuntu trusty main restricted universe multiverse" >> /etc/apt/sources.list'
apt-get update -y -q
apt-get -y -q build-dep manaplus

apt-get install -y -q g++ gcc git xsel make autoconf automake \
    appstream-index libappstream0 libappstream-dev autopoint \
    gettext libxml2-dev libcurl4-gnutls-dev libpng-dev libsdl-gfx1.2-dev \
    libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-net1.2-dev \
    libsdl-ttf2.0-dev gdb valgrind netcat-openbsd procps zlibc

# AppImage - linuxdeploy
# seems to be not possible on docker because security restrictions
#apt-get install -y -q libfuse2