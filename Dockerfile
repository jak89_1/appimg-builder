FROM ubuntu:trusty

WORKDIR /app

COPY install_dependencies.sh /app

COPY . /app/local

RUN apt-get update -qq

RUN chmod +x install_dependencies.sh

RUN chmod +x ./local/linuxdeploy-x86_64.AppImage

RUN ./local/linuxdeploy-x86_64.AppImage --appimage-extract

RUN mv squashfs-root ./local/squashfs-root

RUN ./install_dependencies.sh

# optionaly make the local build script executable 
RUN chmod +x /app/local/build.sh
